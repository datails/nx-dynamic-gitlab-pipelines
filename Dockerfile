FROM node:16

ARG SERVICE
ENV SERVICE ${SERVICE}

WORKDIR /home/usr/app

COPY . .

RUN npm ci

EXPOSE 3000

CMD npx nx serve ${SERVICE}
