/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import * as express from 'express';

const app = express();

app.get('/api', (req, res) => {
  res.send({ message: 'Welcome to baz-service!' });
});

const port = 3333;
const server = app.listen(port, '0.0.0.0', () => {
  console.log(`Listening at http://0.0.0.0:${port}/api`);
});
server.on('error', console.error);
